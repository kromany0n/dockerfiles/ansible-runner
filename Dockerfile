ARG DOCKERFILE_FROM_PYTHON_VERSION=3.12.2-slim-bullseye
FROM python:$DOCKERFILE_FROM_PYTHON_VERSION

ARG ANSIBLE_CORE_VERSION=2.16.3
ARG ANSIBLE_VERSION=9.2.0
ARG ANSIBLE_LINT_VERSION=24.2.0
ARG JMESPATH_VERSION=1.0.1
ARG PYTEST_TESTINFRA_VERSION=10.0.0
ARG MOLECULE_VERSION=24.2.0
ARG MOLECULE_PLUGINS_VERSION=23.5.3

RUN pip install --no-cache-dir \
    ansible==$ANSIBLE_VERSION \
    ansible-lint==$ANSIBLE_LINT_VERSION \
    ansible-core==$ANSIBLE_CORE_VERSION \
    jmespath==$JMESPATH_VERSION \
    pytest-testinfra==$PYTEST_TESTINFRA_VERSION \
    molecule==$MOLECULE_VERSION \
    molecule-plugins==$MOLECULE_PLUGINS_VERSION

RUN apt-get update && apt-get install --no-install-recommends -y git openssh-client && rm -rf /var/lib/apt/lists/*

ENTRYPOINT /bin/bash
